console.log("Hello from JS");

// [SECTION 1] Assignment Operators

//1. Basic Assignment Operator (=)
//this allows us to add the value of the right operand to a variable and assigns the result to the variable.
let assignmentNumber = 5;

let message = 'This is the message';

// 2. Addition assignment operator (+=)
// The addition assignment operator adds the value of the right operand to a variable and assigns the result to the variable.
// assignmentNumber = assignmentNumber + 2;
console.log("Result of the operation: " + assignmentNumber)

// shorthanded version of the statement above
assignmentNumber += 2;
console.log("Result of the operation: " + assignmentNumber)

//[SECTION 1: SUB 2] Arithmetic Operators
//(+, -, *, /)
// 3. Subtraction/Multiple/Division Assignment Operator (-/ */ "/" =).
assignmentNumber *= 4;
console.log("Result of the operation: " + assignmentNumber)

// [RECAP] Assignment Operators

let value = 8;

//Addition assignment (+=)
// console.log(value += 15); //23

// Subtraction Assignment (-=)
// value -= 5; //3

//Multiplication Assignment (*=)
// value *= 2; //16

//Division Assignment (/=)
value /= 2; //4

console.log(value)

//[SECTION] Arithmetic Operators

let x = 15;
let y = 10;

//Addition (+)
let sum = x + y;

console.log(sum); 

//subtraction (-)
let difference = x - y;
console.log(difference);

//multiplication (*)
let product = x * y;
console.log(product);

//division (/)
let quotient = x / y;
console.log(quotient);

// remainder between the 2 values(Modulus "%")
let remainder = x % y;
console.log(remainder) 

//[SUBSECTION] Multiple Operators and Parenthesis

//when multiple operator are applied in a single statment, it follows the PEMDAS rule.

let mdas = 1 + 2 - 3 * 4 / 5;
console.log(mdas); 

// the operations were done in the following order to get to the final answer.
  //1. 3 * 4 = 12
  //2. 12 / 5 = 2.4
  //3. 1 + 2 = 3
  //4. 3 - 2.4 = 0.6


 // NOTE: The order of operations can be change by adding parenthesis to the logic.

 let pemdas = 1 + (2 - 3) * (4 / 5)
 console.log(pemdas);

// [SECTION] Increment and Decrement

let z = 1;

//Pre and Post 
//Increment (++)

//Pre-Increment (syntax: ++variable)

//the value of "z" is added by a value of 1 before returning the value and storing it inside a new variable: "preIncrement".
let preIncrement = ++z; // z + 1
console.log(preIncrement); //result of the pre-increment
//we can see here that the value of "z" was also increased even though we did NOT implicitly specified any variable reassignment.
console.log(z); //2

//Post-Increment (syntax: variable++)
let postIncrement = z++;
//the value of "z" is returned and stored inside the variable called "postIncrement". the value of "z" is at 2 before it was incremented 
console.log(postIncrement);
console.log(z);
//1 + z VS z + 1

//Decrement(--) 
//Pre-decrement (syntax: --variableName)
//the value of "z" starts with 3 before it was decremented.
let preDecrement = --z; //3 - 1
console.log(preDecrement); //2

//Post-decrement (syntax: variable--)
let postDecrement = z--;
//the value of "z" is returned and stored inside a new variable before it will be decremented.
console.log(postDecrement);
console.log(z);

let bagongValue = 3;
let newValue = bagongValue++;
console.log(newValue);
console.log(bagongValue);

//real life situations in programming where we use this type of operation:

  //1.queues
  //2.creating loop conditions

  //[SECTION:] TYPE COERCION

  let numberA = 6;
  let numberB = '6';

//check the data types of the values above
//typeof expression will allow us to identify the data type of a certain value/component.
console.log(typeof numberA); //number
console.log(typeof numberB); //string
//number + string // the number data type was converted into a string to perform concatenation instead of addition.
let coercion = numberA + numberB;
console.log(typeof coercion); //66

//Adding number and boolean
let expressionC = 10 + true; // 10 + 1
console.log(expressionC); //11

let a = true
console.log(typeof a); //boolean
let b = 10;
console.log(typeof b);//number

let expressionD = true + true;
console.log(expressionD); //2

//Note: the boolean value of "true" is also associated with the value of 1. False = 0
// the boolean value of "false" is associated with a value of 0.

//Number with a null value.

let expressionG = 8 + null; //8 (string,number,boolean)
console.log(expressionG);
let d = null;
console.log(typeof d);

//Conversion Rules:
 //1.If atleast one operand is an object, it will be converted into a primitive value or data type.
 //2.After conversion, if atleast 1 operand is a string data type, the 2nd operand is converted into another string to perform concatenation.
 //3.In other cases where both operands are converted to numbers then an arithmetic operation is executed.

 //the object data type which is "null" operand was converted into a primitive data type.

 //String with a null data type
 let expressionF = "Batch145" + null; //null will be converted into a primitive data type.
 console.log(expressionF); //Batch145null

 //1. Batch145 --> string data type

 //Number with undefined

 let expressionE = 9 + undefined;
 console.log(expressionE); //NaN
 let e = undefined;
 console.log(typeof e);

 //1.undefined was converted into a number data type NaN
 //2.9 + NaN = NaN

 //[SECTION] Comparsion Operators
 let name = 'Juan'

    //[SUB SECTION] EQUALITY OPERATOR (==)
    // attempts to CONVERT and COMPARE operands with 2 different data types.
    //returns a boolean value (true or false)
    console.log(1 == 1); //true
    console.log(1 == 2);//false
    console.log(1 == '1');//true
    console.log(1 == true);//true
    console.log(1 == false);//false
    console.log(name == 'Juan');//true
    console.log('Juan' == 'juan'); //fase (case sensitive)
    // console.log('Juan' == Juan); this is an error because the variable was not yet declared.

    //[SUB SECTION] Inequality Operator (!=) ("!" will denote a 'NOT' statement)
    // checks wether the operands are NOT EQUAL or HAVE different values

    console.log(1 != 1); //false
    console.log(1 != 2); //true
    console.log(1 != '1');//false
    console.log(0 != false);//false
    let juan = 'juan';
    console.log('juan' != juan);//false

    //[SUB SECTION] "STRICT" EQUALITY OPERATORS(===)
    //checks wether the operands are equal or have the same value.
    //ALso compares if the data types are the same.

    console.log(1 === 1); //true
    console.log(1 === '1');//false
    console.log(0 === false);//false
    //they have different data types hence, false.

    //[SUB SECTION] "STRICT" INEQUALITY OPERATOR (!==)
    //this will check if the operands are NOT EQUAL or they HAVE DIFFERENT values/content.
    //checks both values and data types of both components or operands.

    console.log(1 !== 1); //false
    console.log(1 !== 2); //true
    console.log(1 !== '1');//true
    console.log(0 !== false); //true

    //Developer's tip: upon creating conditions or statement it is strongly recommended to use "strict" equality operators over "loose" equality operators because it will be easier for us to predetermined outcomes and results in any given scenario.

    //[SECTION] Relational Operators
    let priceA = 1800;
    let priceB = 1450;

    //less than operator
    console.log(priceA < priceB); //false

    //greater than operator
    console.log(priceA > priceB); //true

    let expressionI = 150 <= 150;
    console.log(expressionI); //true

    //Developer's Tip: When writing down/selecting variable names that would describe or contain a boolean value, it is a writing convention for developers to add a prefix of "is" or "are" together with the variable name to form a variable similar on how to answer a simple yes or no question.

    // isSingle = true;

    isLegalAge = true;
    isRegistered = false;

    //for the person to be able to votem both requirements has to be met.

    //we need to use the Proper Logical operator
    //AND (&& Double Ampersand) all criteria has to be MET.
   let isAllowedToVote = isLegalAge && isRegistered
   console.log('Is the person allowed to vote?: ' + isAllowedToVote);

   //OR (|| Double Pipe) - atleast 1 criteria has to be MET, in order to pass.
   let isAllowedForVaccination = isLegalAge || isRegistered;
   console.log("Did the person pass?: " + isAllowedForVaccination );


   //NOT (! Exclamation Point) OPERATOR
     // This will convert or return the opposite value.
     let isTaken = true;
     let isTalented = false;
     console.log(!isTaken);
     console.log(!isTalented);